# PHP-CSM

Dockerized PHP CSM application with MySQL Database connection 

## Planned steps:
* initial idea and concept
* dockerization
* finalization

## Debugging

* append "?XDEBUG_SESSION_START=IDEA_DEBUG" to URL 

## Resources
* Blog Post bootstrap template: https://startbootstrap.com/templates/blog-post/
* Admin Page bootstrap template: https://startbootstrap.com/templates/sb-admin/