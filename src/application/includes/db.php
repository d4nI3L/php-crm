<?php
// db connection params as constants
define('DB_HOST', 'host=db');
define('DB_USR', 'user=usr');
define('DB_PW', 'password=mypassword');
define('DB_NAME', 'dbname=mydb');
define('DB_PORT', 'port=5432');
define('DB_CONNECTION',  DB_HOST . " " . DB_PORT . " " . DB_NAME . " " . DB_USR . " " . DB_PW);

$connection = pg_connect(DB_CONNECTION);

/*if($connection) {
    echo "yes";
}*/