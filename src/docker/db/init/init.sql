CREATE TABLE categories ( id SERIAL NOT NULL PRIMARY KEY, title VARCHAR(255));
CREATE TABLE users ( id SERIAL NOT NULL PRIMARY KEY, name VARCHAR(255), password VARCHAR(255));

INSERT INTO users(name, password) VALUES ('root', 'root');

INSERT INTO categories(title) VALUES ('JavaScript');
INSERT INTO categories(title) VALUES ('Docker');
INSERT INTO categories(title) VALUES ('PHP');
INSERT INTO categories(title) VALUES ('Bootstrap');

